var episode = require("../../modules/table_episode");
var passport = require('passport');
var year = require("../../modules/table_year");
var cat = require("../../modules/table_cat");
var catEpi = require("../../modules/table_catepi");
var ObjectId = require('mongodb').ObjectId;
var fs = require('fs');
var filePath = './public/images/image_avatar/';
var chapter = require("../../modules/table_chapter");
var slug = require('vietnamese-slug');
exports.get_dashboard = function(req, res, next){
    res.render('admin/dashboard', {
        pageTitle: req.__('Bảng điều khiển'),
        layout: false
    });
};
//=============================================ADD MOVIE=======================================//

exports.get_addMovie = function (req, res, next) {
    res.render('admin/index/ListMovie/addMovie', {
        pageTitle: req.__('Thêm mới phim'),
        csrfToken: req.csrfToken(),
        layout:false,
        success: req.flash('success'),
        err: req.flash('err')
    });
};

exports.post_addMovie = (episode_season,episode_back,episode_image,episode_id,episode_name,episode_film,episode_info,year_id,cat_id,episode_hide) =>
    new Promise((resolve, reject) => {
        episode.findOne({"episode_id": episode_id})
            .then(epi =>{
                if(epi){
                    console.log('1.1');
                   reject({status: 400, err:"Trùng ID! Vui lòng nhập lại ID khác."});
                }else{
                    console.log('1.2');
                    var d = new Date();
                    var timeStamp = d.getTime();
                    var MovieNew = new episode({
                        episode_id : episode_id,
                        episode_name: episode_name,
                        episode_name_ascii: slug(episode_name),
                        episode_film: episode_film,
                        episode_info: episode_info,
                        episode_hide: episode_hide,
                        episode_image: episode_image,
                        episode_back:episode_back,
                        create_at: timeStamp,
                        episode_season:episode_season
                    });
                    MovieNew.save();
                    //'5b7bc1e28a3beb18b83d6dac','5b7bc1e28a3beb18b83d6dad',//chuỗi hiện có
                    //"\'5b7bc1e28a3beb18b83d6dac\',\'5b7bc1e28a3beb18b83d6dad\'," //ep vào [] thành
                    //var arr = ['5b7bc1e28a3beb18b83d6dac','5b7bc1e28a3beb18b83d6dad',];// cần

                    var arr = cat_id.split(',');
                    console.log("ARR: "+arr);
                    for (var i= 0;i<arr.length;i++){
                        var saveCat = new catEpi({
                            cat_id: arr[i],
                            episode_id: episode_id,
                            year_id:year_id
                        });
                        saveCat.save();
                    }

                    // cat.update({_id:{$in: arr}},{$push: {"listEpisode": MovieNew._id}},
                    //     {safe: true, upsert: true, new: true,multi: true},
                    //     function (err) {
                    //         console.log("Cread Category");
                    //         resolve({status: 201, msg: "Tạo mới thành công!"});
                    //     });
                    // year.findByIdAndUpdate(year_id,{$push: {"year_order": MovieNew._id}},
                    //     {safe: true,upsert:true, new: true},
                    //     function (err) {
                    //         console.log("Cread Year");
                    //         resolve({status: 201, msg: "Tạo mới thành công!"});
                    //     })

                   // .then(() =>{
                   //      episode.findByIdAndUpdate({"_id": ObjectId(MovieNew._id)},{$push: {"listEpisode": arr,"year_order": year_id}},
                   //          {safe: true, upsert: true, new: true},
                   //          function (err) {
                   //              console.log("ADD list");
                   //              resolve({status: 201, msg: "Tạo mới thành công!"});
                   //          })
                   //
                   //  })
                   // .catch(err =>{
                   //     console.log('1.5');
                   //     reject({status: 500, err: "Lỗi Server !"});
                   // })
                    resolve({status: 200, msg: "Tạo mới thành công!"});
                }
            })
            .catch(err =>{
                console.log('1.5');
                reject({status: 500, err: "Lỗi Server !"});
            });
    });

exports.get_listMovie = function(req, res, next){
    episode.find().sort({_id: -1})
        .then(function(doc){
            res.render('admin/index/listMovie/listMovie', {
                pageTitle: req.__('Danh sách phim'),
                csrfToken: req.csrfToken(),
                layout:false,
                item:doc,
                success: req.flash('success'),
                err: req.flash('err')
            });
        });
};

exports.get_editMovie = (episode_id) =>
    new Promise((resolve, reject) =>{
        episode.findOne({"episode_id": episode_id})
            .then(episo => {
                if(episo.length === 0){
                    console.log("1.1");
                    reject({status:  404, err: 'ID không tồn tại!'});
                }else{
                    console.log("DU LIEU: "+ episo);
                    catEpi.find({"episode_id": episode_id})
                        .then(catEpiID =>{
                            if(catEpiID.length === 0){
                                reject({status:  404, err: 'ID không tồn tại!'});
                            }else{
                                var listCat = [];
                                for(var i=0;i<  catEpiID.length;i++)
                                {
                                    listCat.push({"cat_id": catEpiID[i].cat_id});
                                }
                                resolve({status: 200, item1:episo,item2:JSON.stringify(listCat)});
                            }
                        })
                        .catch(err=>{
                            reject({status: 500,err: "Lỗi server"});
                        });
                    // resolve({status: 200, item:episo});
                }
            })
            .catch(err =>{
                reject({status: 500,err: "Lỗi server"});
            });
    });

exports.get_delectMovie = episode_id =>
    new Promise((resolve, reject) => {
        console.log('episode_id: '+episode_id);
        episode.remove({'episode_id': episode_id})

            .then(doc => {
                var episode_back = doc.episode_back;
                var episode_image = doc.episode_image;
                fs.unlinkSync(filePath+episode_image);
                fs.unlinkSync(filePath+episode_back);

                catEpi.remove({'episode_id': episode_id})
                .then(()=>{
                    console.log('Xoa catEpo');
                    resolve({status: 200, msg: "Đã xóa year!"});
                })
                .catch(err =>{
                    console.log('2.1');
                    reject({status: 500, err: "Thất bại !"});
                });
                console.log('1.1');
                resolve({status: 200, msg: "Đã xóa !"});
            })
            .catch(err =>{
                console.log('2.1');
                reject({status: 500, err: "Thất bại !"});
            });
    });
//=========================================END ADD MOVIE========================================//

//=================================YEAR======================================//
exports.get_addYear =  function(req, res, next){
    res.render('admin/index/listYear/addYear', {
        pageTitle: req.__('Thêm mới năm'),
        csrfToken: req.csrfToken(),
        layout:false,
        success: req.flash('success'),
        // errors: req.session.errors,
        err: req.flash('err')
    });
    // req.session.errors = null;
};

exports.post_addYear = (year_id, year_name) =>
new Promise((resolve, reject) => {
    year.findOne({"year_id": year_id})
        .then(req =>{
            if (req){
                reject({status: 400, err: 'Trùng ID! Vui lòng nhập lại ID khác.'});
            } else {
                var newYear = new year({
                    year_id: year_id,
                    year_name: year_name
                });
                newYear.save()
                    .then(result =>{
                        resolve({status: 200, msg: 'Tạo mới thành công!'});
                    })
                    .catch(err =>{
                        reject({status: 500, err: 'Tạo mới không thành công!'});
                    });
            }
        })
    .catch(err =>{
        reject({status: 500, err: "Lỗi Server !"});
    })
});

exports.get_listYear = function(req, res, next){
    year.find().sort({_id: -1})
        .then(function(doc){
            res.render('admin/index/listYear/listYear', {
                pageTitle: req.__('Năm phát hành'),
                csrfToken: req.csrfToken(),
                layout:false,
                item:doc,
                success: req.flash('success'),
                err: req.flash('err')
            });
        });
};

exports.get_editYear = (_id) =>
    new Promise((resolve, reject) =>{
        year.findOne({"year_id": _id})
            .then(year => {
                if(year.length === 0){
                    reject({status:  404, err: 'ID không tồn tại!'});
                }else{
                    resolve({status: 200, item:year});
                }
            })
            .catch(err =>{
               reject({status: 500,err: "Lỗi server"});
            });
    });

exports.post_edityear = (year_id, year_name, id) =>
    new Promise((resolve, reject) => {
        console.log('Year_name: '+ year_name);
        year.findOne({'year_id':year_id})
            .then(() =>{
                year.findByIdAndUpdate({'_id': ObjectId(id)},{$set: {year_name: year_name}})
                .then(() =>{
                    console.log('1.1');
                    resolve({status: 200, msg: "Cập nhật thành công !"});
                })
                .catch(err =>{
                    console.log('1.2');
                    reject({status: 500, err: "Thất bại !"});
                })
            })
            .catch(err =>{
                console.log('2.1');
                reject({status: 500,err: "Lỗi server"});
            });
    });

exports.get_delectYear = (id) =>
    new Promise((resolve, reject) => {
        console.log('id: '+id);
        year.findByIdAndRemove({'_id': ObjectId(id)})
            .then(() => {
                console.log('1.1');
                resolve({status: 200, msg: "Đã xóa !"});
            })
            .catch(err =>{
                console.log('2.1');
                reject({status: 500, err: "Thất bại !"});
            });
    });
//=================================YEAR==================================//
//=================================CATALOG==================================//

exports.get_addCat =  function(req, res, next){
    res.render('admin/index/listCatalog/addCat', {
        pageTitle: req.__('Thêm mới thể loại'),
        csrfToken: req.csrfToken(),
        layout:false,
        success: req.flash('success'),
        err: req.flash('err')
    });
};

exports.post_addCat = (cat_id, cat_name_title) =>
    new Promise((resolve, reject) => {
        cat.findOne({"cat_id": cat_id})
            .then(req =>{
                if (req){
                    reject({status: 400, err: 'Trùng ID! Vui lòng nhập lại ID khác.'});
                } else {
                    var d = new Date();
                    var timeStamp = d.getTime();
                    var newCat = new cat({
                        cat_id: cat_id,
                        cat_name_title: cat_name_title,
                        cat_name_ascii: slug(cat_name_title)
                    });
                    newCat.save()
                        .then(result =>{
                            resolve({status: 200, msg: 'Tạo mới thành công!'});
                        })
                        .catch(err =>{
                            reject({status: 500, err: 'Tạo mới không thành công!'});
                        });
                }
            })
            .catch(err =>{
                reject({status: 500, err: "Lỗi Server !"});
            })
    });

exports.get_listCat = function(req, res, next){
    cat.find().sort({_id: -1})
        .then(function(doc){
            res.render('admin/index/listCatalog/listCat', {
                pageTitle: req.__('Thể loại'),
                csrfToken: req.csrfToken(),
                layout:false,
                item:doc,
                success: req.flash('success'),
                err: req.flash('err')
            });
        });
};

exports.get_editCat = (_id) =>
    new Promise((resolve, reject) =>{
        cat.findOne({"cat_id": _id})
            .then(cat => {
                if(cat.length === 0){
                    reject({status:  404, err: 'ID không tồn tại!'});
                }else{
                    resolve({status: 200, item:cat});
                }
            })
            .catch(err =>{
                reject({status: 500,err: "Lỗi server"});
            });
    });

exports.post_editCat = (cat_id, cat_name_title,id) =>
    new Promise((resolve, reject) => {

        cat.findOne({'cat_id': cat_id})
            .then(() =>{
                cat.findByIdAndUpdate({'_id': ObjectId(id)},{$set:
                        {cat_name_title: cat_name_title,cat_name_ascii:slug(cat_name_title)}})
                    .then(() =>{
                        console.log('1.1');
                        resolve({status: 200, msg: "Cập nhật thành công !"});
                    })
                    .catch(err =>{
                        console.log('1.2');
                        reject({status: 500, err: "Thất bại !"});
                    })
            })
            .catch(err =>{
                console.log('2.1');
                reject({status: 500,err: "Lỗi server"});
            });
    });

exports.get_delectCat = (id) =>
    new Promise((resolve, reject) => {
        console.log('id: '+id);
        cat.findByIdAndRemove({'_id': ObjectId(id)})
            .then(() => {
                console.log('1.1');
                resolve({status: 200, msg: "Đã xóa !"});
            })
            .catch(err =>{
                console.log('2.1');
                reject({status: 500, err: "Thất bại !"});
            });
    });
//=============================END CATALOG==================================//
//================================CHAPTER======================================//
exports.get_addChapter = (chapter_id,_id) =>
    new Promise((resolve,reject) =>{
        episode.findOne({'_id': ObjectId(_id)})
            .populate({path: "episode_order"})
            //noi bang chapter
            .then(chapter => {
                if(chapter.length === 0){
                    reject({status:  404, err: 'ID không tồn tại!'});
                }else{
                    resolve({status: 200, item:chapter});
                }
            })
            .catch(err =>{
                reject({status: 500,err: "Lỗi server"});
            });
    });

exports.post_addChapter = (chapter_id, idMovie, chapter_url,chapter_num) =>
    new Promise((resolve, reject) => {
        console.log(chapter_id+"/"+idMovie+"/"+chapter_url+"/"+chapter_num);
        chapter.findOne({"chapter_id": chapter_id})
            .then(req =>{
                console.log('vao');
                    var d = new Date();
                    var timeStamp = d.getTime();
                    var newChap = new chapter({
                        chapter_id: chapter_id,
                        chapter_url: chapter_url,
                        chapter_num: chapter_num,
                        create_at: timeStamp
                    });
                    console.log(newChap);
                    newChap.save()
                        .then(result =>{
                            chapter.findByIdAndUpdate({"_id": ObjectId(newChap._id)},{$push: {"listEpisode": idMovie}},
                                {safe: true, upsert: true, new: true,multi:true},
                                function (err) {
                                    console.log("ADD list");
                                    resolve({status: 201, msg: "Tạo mới thành công!"});
                                });
                            episode.findByIdAndUpdate({"_id": ObjectId(idMovie)}, {$push: {"episode_order":newChap._id}},
                                {safe: true, upsert: true, new: true},
                                function (err) {
                                    console.log("ADD Episode");
                                    resolve({status: 201, msg: "Tạo mới thành công!"});
                                });
                        })
                        .catch(err =>{
                            reject({status: 500, err: 'Tạo mới không thành công!'});
                        });
            })
            .catch(err =>{
                reject({status: 500, err: "Lỗi Server !"});
            })
    });

exports.get_deleteChapter = (id,idMovie) =>
    new Promise((resolve, reject) => {
        console.log('idMovie: '+idMovie);
        chapter.findByIdAndRemove({'_id': ObjectId(id)})
            .then(() => {
                episode.findByIdAndUpdate(idMovie,
                    {$pull: {"episode_order": id}},
                    {safe: true, upsert: true, new:true},
                    function (err) {
                        console.log("DELE list");
                        resolve({status: 201, msg: "Đã xóa !"});
                       });
                console.log('1.1');
                resolve({status: 200, msg: "Đã xóa !"});
            })
            .catch(err =>{
                console.log('2.1');
                reject({status: 500, err: "Thất bại !"});
            });
    });

exports.get_editChap = (chapter_id) =>
    new Promise((resolve, reject) =>{
        chapter.findOne({"chapter_id": chapter_id})
            .populate({path: "listEpisode"})
            .then(chap => {
                if(chap.length === 0){
                    reject({status:  404, err: 'ID không tồn tại!'});
                }else{
                    resolve({status: 200, item:chap});
                }
            })
            .catch(err =>{
                reject({status: 500,err: "Lỗi server"});
            });
    });

exports.post_editChapter = (chapter_url, chapter_num,id) =>
    new Promise((resolve, reject) => {

        chapter.findOne({'_id': ObjectId(id)})
            .then(() =>{
                chapter.findByIdAndUpdate({'_id': ObjectId(id)},{$set: {chapter_url: chapter_url,chapter_num:chapter_num}})
                    .then(() =>{
                        console.log('1.1');
                        resolve({status: 200, msg: "Cập nhật thành công !"});
                    })
                    .catch(err =>{
                        console.log('1.2');
                        reject({status: 500, err: "Thất bại !"});
                    })
            })
            .catch(err =>{
                console.log('2.1');
                reject({status: 500,err: "Lỗi server"});
            });
    });
//=============================END CHAPTER====================================//



exports.get_Logout = function(req, res, next){
    req.logOut();
    res.redirect('/admin/login');
};

exports.login_get = function (req, res, next) {
    var messages = req.flash('error');
    res.render('admin/login',{
        pageTitle: req.__('Đăng nhập quản trị viên'),
        csrfToken: req.csrfToken(),
        messages: messages,
        hasErrors: messages.length > 0,
        layout: false
    });
};

exports.login_post = passport.authenticate('admin.login', {
    successRedirect: '/admin/',
    failureRedirect: '/admin/login',
    badRequestMessage: 'Các trường điều bắt buộc',
    failureFlash: true
});

exports.notLogin_use = function (req, res, next) {
    next();
};

exports.isLoggedIn = function (req, res, next) {
    if(req.user && req.user.roles === 'ADMIN' && req.user.provider === 'admin'){
        return next();
    }else {
       return res.redirect('/admin/login');
    }
};

exports.notLoggedIn = function (req, res, next) {
    if(!req.user){
        return next();
    }else {
        if(req.user && req.user.roles !== 'ADMIN' && req.user.provider !== 'admin'){
            return next();
        }else {
            return res.redirect('/admin');
        }
    }
};